FROM node:alpine AS install
COPY package*.json ./app/
WORKDIR /app
RUN npm install
COPY . .

FROM node:alpine AS start
WORKDIR /app
COPY --from=install /app .
CMD ["node", "index.js"]